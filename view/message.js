// TODO: Use html template.

module.exports = function(req, res) {
	if (!req.params.id) res.status(500).json({result: 'failed'});

	res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(getHTML(req.params.id));
    res.end();
};

var getHTML = function(id) {
	return "<script src='https://code.jquery.com/jquery-3.0.0.min.js' integrity='sha256-JmvOoLtYsmqlsWxa7mDSLMwa6dZ9rrIdtrrVYRnDRH0=' crossorigin='anonymous'></script> \
			<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' integrity='sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7' crossorigin='anonymous'> \
			<label> Get the secret message </label> \
			<div> \
				<label id='decide-msg-title'> password: </label> \
				<label style='display:none' id='decoded-msg'></label> \
				<input type='text' placeholder='password' id='decode-pw'></input> \
				<button class='btn btn-default' id='decode-btn' onclick='decodeClicked()'>decode</button> \
			</div> \
			<script> \
				$(document).ready(function() { \
					$.get('http://localhost:9145/retrive/"+id+"/status', \
					function(data) { \
						var btn = $('#decode-btn'); \
						if (data.status) btn.attr('disabled', false); \
						else { \
							$('#decode-pw').attr('placeholder', 'mail not sent.'); \
							btn.attr('disabled', true); \
						} \
					}); \
				}); \
				var decodeClicked = function() { \
					if (!$('#decode-pw').val()) return; \
					$.post('http://localhost:9145/retrive/"+id+"/msg', {password: $('#decode-pw').val()}, \
					function(data) { \
						$('#decode-pw').hide(); \
						$('#decoded-msg').show().html(data.msg); \
						$('#decode-btn').hide(); \
						$('#decide-msg-title').html('message: '); \
					}) \
					.fail(function() { \
						$('#decode-pw').val(''); \
						$('#decode-pw').attr('placeholder', 'failed. please try again.') \
					}) \
				} \
			</script>";
};