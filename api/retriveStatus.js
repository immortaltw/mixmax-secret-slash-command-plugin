var sync = require('synchronize');

/**
 * For verifying and setting status of a message.
 * If a mail has not been sent, the 'sent' property is set to false by default.
 * If sent, set it to true.
 */
module.exports = function(req, res) {
	var content;
	var levelDB = req.db;

	// If not param, exit early.
	if (!req.params.id) return res.status(500).json({result: 'failed'});

	// Get status from db.
	try {
		response = sync.await(levelDB.get(req.params.id, sync.defer()));
	} catch (e) {
		console.error(e);
		return res.json({result: 'failed'});
	}

	// Fill content.
	if (response) {
		content = JSON.parse(response);
	} else {
		return res.status(500).json({result: 'failed'});
	}

	// Special case. Use this to update the status.
	var term = typeof req.query.text === 'string'? req.query.text.trim(): undefined;
	if (term && term === "true") {
		content.sent = true;
		levelDB.put(req.params.id, JSON.stringify(content), function(err) { console.error(err);});
	}

	return res.json({result: "success", status: content.sent});
};