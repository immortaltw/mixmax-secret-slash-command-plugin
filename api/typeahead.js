
module.exports = function(req, res) {
	var term = req.query.text.trim();

	// If query string is empty.
	if (!term) {
		res.json = ([{
			title: "<i>(enter message and password)</i>",
			text: "[message] [password]"
		}]);
		return;
	}

	// Prepare the returning json.
	var result = {
		titile: "",
		text: ""
	};

	var termArr = term.split();
	// Always use the first two items no matter how big the array is.
	if (termArr.length > 2) termArr.slice(0, 2);
	// If the first query string is typed (msg).
	if (termArr.length === 1) {
		result.title = "<i>(enter password)</i>",
		result.text = termArr[0]
	}
	// If the second query string is typed (password).
	else {
		result.title = "<i>Ready!!</i>",
		result.text = termArr[0] + " " + termArr[1]
	}

	// Resolve the result.
	res.json([result]);
};