var sync = require('synchronize');

/**
 * This module is used to retrive msg if obtained correct password
 * from the POST object.
 */
module.exports = function(req, res) {
	console.log("retrive");

	if (!req.params.id || !req.body) return res.status(500).json({result: 'failed'});
	
	// Declare variables.
	var response;
	var message;
	var sentPassword = req.body.password || "";

	// Get the content of the id from db.
	var levelDB = req.db;
	try {
		response = sync.await(levelDB.get(req.params.id, sync.defer()));
	} catch (e) {
		console.error(e);
		return res.json({result: 'failed'});
	}

	// Check if the password sent from the POST req == password in db.
	if (response) {
		var content = JSON.parse(response);
		var password = content.password;
		message = content.msg;
		console.log(sentPassword+" "+password);
		if (sentPassword !== password) return res.status(500).json({result: 'failed'});
	}
	res.json({result: "success", msg: message});
};