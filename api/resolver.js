var sync = require('synchronize');
var request = require('request');
var uuid = require('node-uuid');

/**
 * This module is in charge of storing msg and password with a unique key
 * into the db. It also generate html to be displayed in the mail.
 */
module.exports = function(req, res) {
	// Parse the result text.
	var term = req.query.text.trim();
	var termArr = term.split(" ");
	if (termArr.length != 2) return res.status(500).send('Error');
	var _msg = termArr[0];
	var _password = termArr[1];

	// Create a record in db with the result text.
	var levelDB = req.db;
	var id = uuid.v1({msec: new Date().getTime()});
	var content = JSON.stringify({msg: _msg, password: _password, sent: false});

	// Create a new record with the uuid.
	levelDB.put(id, content, function(err) { console.error(err);});
	levelDB.get(id, function(err, value) {
		if (err) { return console.error("unable to query id " + id); }
		return console.error("id already exists. " + value);
	});

	// Generate HTML for the message
	var html = generateReturnHTML(id);
	res.json({body: html});
};

var generateReturnHTML = function(id) {
	return "<script   src='https://code.jquery.com/jquery-3.0.0.min.js'   integrity='sha256-JmvOoLtYsmqlsWxa7mDSLMwa6dZ9rrIdtrrVYRnDRH0='   crossorigin='anonymous'></script> \
			<div class='msgbox'> \
				<label> Get the secret message </label> \
				<a class='btn btn-default' href='http://localhost:9145/message/"+id+"'>Take me there!</a> \
			</div> \
			<style> \
				a:link,a:visited{text-decoration: none;text-align: center;}.btn,label{display:inline-block}.btn{margin:10px;font-size:14px;line-height:1.42857143}.msgbox{font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;color:#333}label{max-width:100%;margin-bottom:5px;font-weight:700}.btn-default{color:#333;background-color:#fff;border-color:#ccc}.btn{padding:6px 12px;margin-bottom:10;font-weight:400;text-align:center;white-space:nowrap;vertical-align:middle;-ms-touch-action:manipulation;touch-action:manipulation;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;background-image:none;border:1px solid #ccc;border-radius:4px}.btn:hover{background-color: #ccc;}\
			</style> \
			<script> \
				$('.js-send-now', window.parent.document).click(function() { \
					$.get('http://localhost:9145/retrive/"+id+"/status?text=true', function(data) {}); \
				}); \
			</script> \
			";
}