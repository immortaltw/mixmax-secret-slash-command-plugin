# Secret Slash Command for Mixmax

This slash command adds a secret message to the mail. The sender uses this command to attach a message with a password. The recipient uses the same password to unlock the secret message.

## Running locally

1. Install using `npm install`
2. Run using `npm start`

## Usage

type ```/secret [message] [password]```

## TODO

1. I use a very hackey way to determine if a mail has been sent or not, which is a very bad practice. Need to find a better way to obtain this info.
2. Use html template in the message view.
3. Avoid sending message and password in plain text.
