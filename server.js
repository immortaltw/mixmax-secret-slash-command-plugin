var app = require("express")();
var sync = require('synchronize');
var cors = require('cors');
var levelUp = require('level')('./mydb');
var bodyParser = require('body-parser');

// Support json format.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Use fibers in all routes so we can use sync.await() to make async code easier to work with.
app.use(function(req, res, next) {
  sync.fiber(next);
});

// Use level db in all routers.
app.use(function(req, res, next) {
	req.db = levelUp;
	next();
});

// Whitelist the api call.
var corsOptions = {
  origin: /^[^.\s]+\.mixmax\.com$/,
  credentials: true
};

app.get('/message/:id', require('./view/message'));
app.get('/typeahead', cors(corsOptions), require('./api/typeahead'));
app.get('/resolver', cors(corsOptions), require('./api/resolver'));
app.get('/retrive/:id/status', cors(corsOptions), require('./api/retriveStatus'));
app.post('/retrive/:id/msg', cors(corsOptions), require('./api/retriveMsg'));

app.listen(process.env.PORT || 9145);
